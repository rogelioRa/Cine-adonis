'use strict'

const Boleto = use('App/Model/Boleto');
const DB = use('Database');

class AsientosController {

  constructor (socket, request) {
    this.socket = socket
    this.request = request
  }
    
  * onReserve(reserve){
    this.socket.exceptMe().emit("reserve",{no_as:reserve.no_as,sucursal_id:reserve.sucursal_id,reserved:reserve.bool,hora:reserve.hora}); 
  }
    
  * onBuy(buyed){
    var dataEmit = [];
    var dataInsert = [];
    try{
      
      for (var i = 0; i < buyed.no_ass.length; i++){
          dataInsert.push({
              user_id:this.socket.currentUser.id,
              sucursal_id:buyed.sucursal_id,
              horario_id:buyed.horario_id,
              total:buyed.total/buyed.no_ass.length,
              no_aciento:buyed.no_ass[i]
          });
          var hora_inicio = yield DB.from('horarios').where('id','=',buyed.horario_id).select('hora_inicio');
          dataEmit.push({
              no_as:buyed.no_ass[i],
              sucursal_id:buyed.sucursal_id,
              reserved:true,
              hora:hora_inicio[0].hora_inicio
          });
      }
      yield DB
      .insert(dataInsert)
      .into('boleto');

      this.socket.toEveryone().emit("buy",dataEmit);   
    }catch(e){
      console.log(e)
    }
  }
    

}

module.exports = AsientosController
