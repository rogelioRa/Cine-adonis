'use strict'

const Lucid = use('Lucid')

class Boleto extends Lucid {

    static boot () {
        super.boot()
    }
    static get table () {
        return 'boleto'
    }
    static get updateTimestamp () {
        return null
    }
    static get createTimestamp () {
        return null
    }


}

module.exports = Boleto
