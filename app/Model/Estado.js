'use strict'

const Lucid = use('Lucid')

class Estado extends Lucid {

    static boot () {
        super.boot()
    }
    static get table () {
        return 'estados'
    }

}

module.exports = Estado
