'use strict'

const Lucid = use('Lucid')

class Ciudad extends Lucid {

    static boot () {
        super.boot()
    }
    static get table () {
        return 'municipios'
    }

}

module.exports = Ciudad
