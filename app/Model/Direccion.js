'use strict'

const Lucid = use('Lucid')

class Direccion  extends Lucid {

    static boot () {
        super.boot()
    }
    static get table () {
        return 'direcciones'
    }
    static get updateTimestamp () {
        return null
    }
    static get createTimestamp () {
        return null
    }

}

module.exports = Direccion
