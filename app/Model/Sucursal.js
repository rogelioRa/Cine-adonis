'use strict'

const Lucid = use('Lucid')

class Sucursal extends Lucid {

    static boot () {
        super.boot()
    }
    static get table () {
        return 'sucursales'
    }
    static get updateTimestamp () {
        return null
    }
    static get createTimestamp () {
        return null
    }

}

module.exports = Sucursal
