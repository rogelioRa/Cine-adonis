'use strict'

const DB = use('Database')
class IsAdmin {

  * handle (request, response, next) {
    // here goes your middleware logic
    // yield next to pass the request to next middleware or controller
    if(request.currentUser){
      let admin = yield DB.table('role_assigned').join('roles','role_assigned.role_id','=','roles.id')
      .where('user_id','=',request.currentUser.id)
      .select('roles.nombre')

      if(admin[0].nombre !== 'Admin'){
          response.redirect('/')
      }
    }
    yield next
  }

}

module.exports = IsAdmin
