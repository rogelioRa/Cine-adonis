'use strict'

/*
|--------------------------------------------------------------------------
| Router
|--------------------------------------------------------------------------
|
| AdonisJs Router helps you in defining urls and their actions. It supports
| all major HTTP conventions to keep your routes file descriptive and
| clean.
|
| @example
| Route.get('/user', 'UserController.index')
| Route.post('/user', 'UserController.store')
| Route.resource('user', 'UserController')
*/
const User = use("App/Model/User") 
const DB = use('Database')
const Route = use('Route')

Route.get('/',function(req,res){
    if(req.currentUser)
      return  res.sendView('welcome',{User:req.currentUser})
    else
      return  res.sendView('welcome',{User:null})
});

Route.get('/cartelera','CarteleraController.show');
Route.post('/register','UserController.register');
Route.post('/login','UserController.login');
Route.get('/logout','UserController.logout');
Route.post('/ciudades','CiudadController.get');
Route.post('/estados','EstadoController.get');
Route.post('/sucursales','SucursalController.get');
Route.post('/peliculas','CarteleraController.get');
Route.get('/get-asientos','PeliculaController.getAsientosOcupados');
Route.group('auth-routes', () => {
    Route.get('horarios','HorariosController.show').middleware('isAdmin');
    Route.post('horarios-reg','HorariosController.save').middleware('isAdmin');
    Route.post('/get-pelicuas','PeliculaController.get').middleware('isAdmin');
    Route.get('/grafica','PeliculaController.grafica').middleware('isAdmin');
    Route.post('/get-salas','PeliculaController.get_salas').middleware('isAdmin');
    Route.get("getEstadisticas",'CarteleraController.getEstadisticas').middleware('isAdmin');
    Route.post('/get-contextos','PeliculaController.get_contextos').middleware('isAdmin');
    Route.post('/pelicula/save','PeliculaController.save').middleware('isAdmin');
    Route.post('/clasificaciones','ClasificacionController.get').middleware('isAdmin');
    Route.post('/generos','GeneroController.get').middleware('isAdmin');
    Route.get('/reg-sucursal','SucursalController.show').middleware('isAdmin');
    Route.post('/saveSucursal','SucursalController.save').middleware('isAdmin');
    Route.get('/peliculas','PeliculaController.show').middleware('isAdmin');
}).middleware('auth');
