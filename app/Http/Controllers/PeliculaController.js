'use strict'

const Pelicula  = use('App/Model/Pelicula')
const Database  = use('Database')
const Helpers   = use('Helpers')
const Sala      = use("App/Model/Sala")
const Contexto  = use("App/Model/Contexto")
const Boleto  = use("App/Model/Boleto")
class PeliculaController {

    * show(request,response){
      if(request.currentUser){
          let user = request.currentUser;
          let admin = yield Database.table('role_assigned').join('roles','role_assigned.role_id','=','roles.id')
          .where('user_id','=',request.currentUser.id)
          .select('roles.nombre')
          var role = (request.currentUser) ? admin[0].nombre : null
          return yield response.sendView('peliculas',{User:user,role:role})
        }else{
          return yield response.sendView('peliculas',{User:null,role:null})
        }
    }
	* save(request,response){
        const post = request.file('imagen')
        const fileName = `${new Date().getTime()}.${post.extension()}`
        yield post.move(Helpers.publicPath('assets/img/peliculas/'), fileName)

        if (!post.moved()) {
          response.badRequest(post.errors())
          return
        }
        var data ={
            nombre : request.input('nombre'),
            sinopsis : request.input('sinopsis'),
            trailer : request.input('trailer'),
            genero_id : request.input('genero_id'),
            clasificacion_id : request.input('clasificacion_id'),
            duracion : request.input('duracion'),
            imagen : fileName
        }
        const pelicula = new Pelicula(data)
        yield pelicula.save()
        response.json({status:200,data:pelicula})
	}
    * get(request,response){
        const peliculas =  yield Pelicula.all()
        return response.json(peliculas)
    }
    * get_salas(request,response){
        const salas =  yield Sala.all()
        return response.json(salas)   
    }
    * get_contextos(request,response){
        const contextos = yield Contexto.all()
        return response.json(contextos)
    }
    * grafica(request,response){
        if(request.currentUser){
          let user = request.currentUser;
          let admin = yield Database.table('role_assigned').join('roles','role_assigned.role_id','=','roles.id')
          .where('user_id','=',request.currentUser.id)
          .select('roles.nombre')
          var role = (request.currentUser) ? admin[0].nombre : null
          return yield response.sendView('grafica',{User:user,role:role})
        }else{
          return yield response.sendView('grafica',{User:null,role:null})
        }
    }
    
    * getAsientosOcupados(request,response){
        try{
          const asientos = yield Boleto.query().where('horario_id','=',request.input('horario_id')).select('no_aciento','horario_id','sucursal_id')
          var dataEmit = [];
          for (var i = 0; i < asientos.length; i++){
              var hora_inicio = yield Database.from('horarios').where('id','=',asientos[i].horario_id).select('hora_inicio');
              dataEmit.push({
                  no_as:asientos[i].no_aciento,
                  sucursal_id:asientos[i].sucursal_id,
                  reserved:true,
                  hora:hora_inicio[0].hora_inicio
              });
          }
          return response.json(dataEmit)
        }catch(e){
          console.log(e)
        }
    }
    
}

module.exports = PeliculaController
