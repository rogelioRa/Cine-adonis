'use strict'

const DB = use('Database')
const User = use('App/Model/User')
const Validator = use('Validator')
const Direccion = use('App/Model/Direccion')
class UserController {

	* login(request,response){
		const email = request.input('email')
	    const password = request.input('password')
	    const login = yield request.auth.attempt(email, password)
	    if (login) {
	      response.send('Logged In Successfully')
	      return
	    }
	    response.unauthorized('Invalid credentails')
	}
    * logout(request,response){
        yield request.auth.logout()
        return response.redirect('/')
    }
	* register(request,response){
		const rules = {email:'unique:users|email|required',username:'required|unique:users',password:'required',};
		const userData  = request.all();
        const validation  = yield Validator.validate(userData,rules)
        if(validation.fails()){
        	response.json({"message":'fallaron las validaciones',status:401,data:validation.messages()})
        	return 
        }else{
        	const user    = new User()
        	user.username = request.input('username')
        	user.email    = request.input('email')
        	user.password = request.input('password')
        	yield user.save()
        	const assigned = yield DB
    	      	.insert({ user_id: user.id, role_id: 2 })//2 => role general
	            .into('role_assigned')
	            .returning('id')
	        var registerMessage = { success: user }
	        response.json({message:"Usuario registrado con exito.",status:200,data:registerMessage.success})
	        return	
        }
        
	}
}

module.exports = UserController
