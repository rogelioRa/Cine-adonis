'use strict'

const Pelicula = use('App/Model/Pelicula')
const Boleto  = use("App/Model/Boleto")
const DB = use('Database')

class CarteleraController {

	* show(request,response){
        if(request.currentUser){
          let user = request.currentUser;
          let admin = yield DB.table('role_assigned').join('roles','role_assigned.role_id','=','roles.id')
          .where('user_id','=',request.currentUser.id)
          .select('roles.nombre')
          var role = (request.currentUser) ? admin[0].nombre : null
          return yield response.sendView('cartelera',{User:user,role:role})
        }else{
          return yield response.sendView('cartelera',{User:null,role:null})
        }
	}
	* ventasShow(request,response){
		return yield response.sendView("ventas")
	}

    * get(request,response){
        let cartelera = yield Pelicula.query().join("horarios","horarios.pelicula_id","=","peliculas.id")
        .join("salas","horarios.sala_id","=","salas.id")
        .join("contextos","horarios.contexto_id","=","contextos.id")
        .join("generos","peliculas.genero_id","=","generos.id")
        .where("fecha","=",DB.raw('current_date'))
        .where("hora_inicio",">",DB.raw('current_time'))
        .where("sucursal_id","=",request.input('id'))
        .groupBy('peliculas.id')
        .select("peliculas.*",
        DB.raw("string_agg(cast(horarios.hora_inicio as TEXT),',') as horas"),
        DB.raw("string_agg(cast(horarios.id as TEXT),',') as horarios_id"),
        DB.raw("string_agg(DISTINCT generos.nombre,',') as genero"),
        DB.raw("string_agg(salas.nombre,',') as sala"),
        DB.raw("string_agg(contextos.nombre,',') as contextos"),
        DB.raw("string_agg(cast(contextos.costo as TEXT),',') as costos"))

        return response.json(cartelera);

    }
  * getEstadisticas(request,response){
       let estadisticas = yield Boleto.query().join('horarios','horarios.id','=','boleto.horario_id')
                                              .join('contextos','horarios.contexto_id','contextos.id')
                                              .groupBy('horarios.fecha')
                                              .select(DB.raw("(case when (extract(month from fecha)) = 1 then sum(costo) else 0 end) as enero"),
                                                DB.raw("(case when (extract(month from fecha)) = 2 then sum(costo) else 0 end) as febrero"),
                                                DB.raw("(case when (extract(month from fecha)) = 3 then sum(costo) else 0 end) as marzo"),
                                                DB.raw("(case when (extract(month from fecha)) = 4 then sum(costo) else 0 end) as abril"),
                                                DB.raw("(case when (extract(month from fecha)) = 5 then sum(costo) else 0 end) as mayo"),
                                                DB.raw("(case when (extract(month from fecha)) = 6 then sum(costo) else 0 end) as junio"),
                                                DB.raw("(case when (extract(month from fecha)) = 7 then sum(costo) else 0 end) as julio"),
                                                DB.raw("(case when (extract(month from fecha)) = 8 then sum(costo) else 0 end) as agosto"),
                                                DB.raw("(case when (extract(month from fecha)) = 9 then sum(costo) else 0 end) as septiembre"),
                                                DB.raw("(case when (extract(month from fecha)) = 10 then sum(costo) else 0 end) as octubre"),
                                                DB.raw("(case when (extract(month from fecha)) = 11 then sum(costo) else 0 end) as noviembre"),
                                                DB.raw("(case when (extract(month from fecha)) = 12 then sum(costo) else 0 end) as diciembre"))
        return response.json(estadisticas);
  }  
}

module.exports = CarteleraController
