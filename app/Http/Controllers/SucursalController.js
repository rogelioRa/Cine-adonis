'use strict'

const Sucursal = use('App/Model/Sucursal')
const Direccion  = use('App/Model/Direccion')
const Database  = use('App/Model/Ciudad')
const DB  = use('Database')

class SucursalController {

	* get(request,response){

		let sucursal = yield Sucursal.query().join('direcciones','sucursales.direccion_id','direcciones.id')
    .where('direcciones.municipio_id','=',request.input('id')).select('sucursales.id','sucursales.nombre')

        return response.json(sucursal)
	}
	* show(request,response){
        if(request.currentUser){
          let user = request.currentUser;
          let admin = yield DB.table('role_assigned').join('roles','role_assigned.role_id','=','roles.id')
          .where('user_id','=',request.currentUser.id)
          .select('roles.nombre')
          var role = (request.currentUser) ? admin[0].nombre : null
          return yield response.sendView('sucursal',{User:user,role:role})
        }else{
          return yield response.sendView('sucursal',{User:null,role:null})
        }
	}
	* save(request,response){
        let municipio_id = yield Database.query().from('municipios').where('municipio','=',request.input('address')).select('id')
        console.log(municipio_id)
        if(municipio_id){
            let dataD = {
                lat:request.input('lat'),
                lang:request.input('lng'),
                municipio_id:municipio_id[0].id
            }
            let direccion = new Direccion(dataD)
            yield direccion.save();
            let dataS = {
                nombre:request.input('nombre'),
                direccion_id:direccion.id,
            }
            let sucursal = new Sucursal(dataS)
            yield sucursal.save()

            return response.json({status:200,data:sucursal})
        }else{
            return response.json({status:500,data:null})
        }



	}
}

module.exports = SucursalController
