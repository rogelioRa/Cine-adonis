'use strict'

const Ciudad = use('App/Model/Ciudad')
const DB = use('Database')

class CiudadController {

	* get(request,response){
		let ciudad = yield DB.table('estados_municipios').join('municipios','estados_municipios.municipio_id','municipios.id').where('estado_id','=',request.input('id')).select('municipios.id','municipios.municipio')
        return response.json(ciudad)
	}
}

module.exports = CiudadController
