'use strict'

const DB = use('Database')

class GeneroController {

	* get(request,response){
		let genero = yield DB.from('generos')
        return response.json(genero)
	}
}

module.exports = GeneroController
