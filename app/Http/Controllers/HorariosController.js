'use strict'

const Sucursal = use('App/Model/Sucursal')
const Direccion  = use('App/Model/Direccion')
const Peliculas  = use('App/Model/Pelicula')
const Validator = use('Validator')
const Horario = use('App/Model/Horario')
const Database = use('Database')

class HorariosController {

	* get(request,response){

		let sucursal = yield Sucursal.query().join('direcciones','sucursales.direccion_id','direcciones.id')
        .where('direcciones.municipio_id','=',request.input('id')).select('sucursales.id','sucursales.nombre')

        return response.json(sucursal)
	}
	* show(request,response){
		//return response.json(peliculas)
        if(request.currentUser){
          let user = request.currentUser;
          let admin = yield Database.table('role_assigned').join('roles','role_assigned.role_id','=','roles.id')
          .where('user_id','=',request.currentUser.id)
          .select('roles.nombre')
          var role = (request.currentUser) ? admin[0].nombre : null
          return yield response.sendView('horarios',{User:user,role:role})
        }else{
          return yield response.sendView('horarios',{User:null,role:null})
        }
	}
	* save(request,response){
		 const datos = yield request.all()
		 const rules = {
		 	"hora":"required",
		 	"fecha":"required",
		 	"sucursal":"required",
		 	"pelicula":"required",
		 	"sala":"required",
		 	"contexto":"required"
		 };
		 const validator = yield Validator.validate(datos,rules)
		 if(validator.fails()){
		 	return response.json({status:401,message:"Los datos no se validaron correctamente","data":validator.messages()})
		 }else{
		 	const getHorario = yield Horario.query().where("sucursal_id","=",request.input("sucursal"))
		 									.where("sala_id","=",request.input("sala"))
		 									.where("fecha","=",request.input("fecha"))
		 									.where("hora_inicio","=",request.input("hora"))
		 									.select("*");
		 	if(getHorario.length>0){// ya se encuentra una funcion registrada a esa hora en esta sala y en esta sucursal
		 		return response.json({status:402,message:"Funcion no disponible ha esta hora, la sala se encuentra ocupada intenta con otra sala o selecciona otra hora"})
		 	}else{
			 	const horario       = new Horario();
			 	horario.sucursal_id = request.input("sucursal");
			 	horario.sala_id 	= request.input("sala");
			 	horario.contexto_id = request.input("contexto");
			 	horario.pelicula_id = request.input("pelicula");
			 	horario.fecha 		= request.input("fecha")	 
			 	horario.hora_inicio = request.input("hora");
			 	yield horario.save();
			 	response.json({status:200,message:"El horario se ha registrado exitosamente",data:horario})
			 	return
		 	}
		 }
	}
}

module.exports = HorariosController
