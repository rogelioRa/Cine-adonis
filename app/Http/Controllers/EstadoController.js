'use strict'

const Estado = use('App/Model/Estado')

class EstadoController {

	* get(request,response){
		let ciudad = yield Estado.all()
        return response.json(ciudad)
	}
}

module.exports = EstadoController
