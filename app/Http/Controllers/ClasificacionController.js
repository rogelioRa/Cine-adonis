'use strict'

const DB = use('Database')

class ClasificacionController {

	* get(request,response){
		let clasificacion = yield DB.from('clasificaciones')
        return response.json(clasificacion)
	}
}

module.exports = ClasificacionController
