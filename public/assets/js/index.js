let __init = {
  boot:function(){
      let boot=this;
      boot.estados.isset();

      $(this.estados.input).on({
          change:function(){
              boot.ciudades.get();
          }
      });

      $(this.ciudades.input).on({
          change:function(){
              boot.sucursales.get();
          }
      });

      $(this.sucursales.input).on({
          change:function(){
              boot.cartelera.get();
          }
      });

      $("#close_sala").click(function(){
          boot.asientos.clear();
          $("#sala").hide('slow');
          $("#cartelera").show('slow');
      });

      $("body").on('click','a.btn_horario',function(){
          $("#sala").show('slow');
          $("#cartelera").hide('slow');
          let suc = null;
          $("#select_sucursales").children('option').each(function(i,o){if($(o).val() === $("#select_sucursales").val()){ suc = $(o).text()}})
          let info = $(this).parent().parent().parent().data('info');
          console.log(info);
          $("#sala").data('sucursal_id',$(boot.sucursales.input).val());
          $(boot.sucursales.input).prop('selected',true);
          $("#s_sucursal").text(suc);
          $("#s_nombre").text(info.nombre);
          $("#s_func").text($(this).text());
          $("#s_func").data('horario_id',$(this).data('id'));
          $("#s_sinopsis").text(info.sinopsis);
          $("#s_trailer").attr('href',info.trailer);
          $("#s_imagen").attr('src','/assets/img/peliculas/'+info.imagen);
          $("#s_precio").data('selected',0);
          $("#s_precio").data('val',$(this).data('precio')).text($(this).data('precio'));
          boot.asientos.verifyBusy();
      });

      $(".as_sala").click(function(){
          let suc = parseFloat($("#sala").data('sucursal_id'));
          if($(this).data('status') == "desocupado"){
              $(this).attr('src','/assets/img/seat_5.png');
              $(this).data('status','en proceso');
              $("#s_total").data('selected',$("#s_total").data('selected')+1);
              $("#s_as").text($("#s_total").data('selected'));
              $("#s_total").text(parseFloat($("#s_precio").data('val') * $("#s_total").data('selected')).toFixed(2));
              client.emit('reserve',{
                  hora:$("#s_func").text(),
                  sucursal_id:suc,
                  no_as:$(this).parent().data('fila')+$(this).data('num'),
                  bool:true
              });
          }else if($(this).data('status') == "en proceso"){
              $(this).attr('src','/assets/img/desocupado.png');
              $(this).data('status','desocupado');
              $("#s_total").data('selected',$("#s_total").data('selected')-1);
              $("#s_as").text($("#s_total").data('selected'));
              $("#s_total").text(parseFloat($("#s_precio").data('val') * $("#s_total").data('selected')).toFixed(2));
              client.emit('reserve',{
                  hora:$("#s_func").text(),
                  sucursal_id:suc,
                  no_as:$(this).parent().data('fila')+$(this).data('num'),
                  bool:false
              });
          }
      });

      client.on('reserve',function(reserve){
          $('.scenario').children('div').each(function(){ 
              $(this).children('div').eq(1).each(function(){
                  var f = $(this).data('fila');
                  $(this).children('img').each(function(i,a){
                      if($("#select_sucursales").val() == reserve.sucursal_id && reserve.hora == $("#s_func").text()){
                          var num = f+$(a).data('num');
                          if(reserve.no_as === num){
                              if(reserve.reserved){
                                  $(this).attr('src','/assets/img/ocupado.png');
                                  $(this).data('status','ocupado');
                              }else{
                                  $(this).attr('src','/assets/img/desocupado.png');
                                  $(this).data('status','desocupado');
                              }
                          }    
                      }
                  });
              }); 
          });
      });

      client.on('buy',function(buyed){
        boot.asientos.sarch(function(f,obj){
          var num = f+$(obj).data('num');
          $.each(buyed,function(i,b){
              if($(boot.sucursales.input).val() == b.sucursal_id && b.hora == $("#s_func").text()){
                if(b.no_as === num){
                  if(b.reserved){
                      $(obj).attr('src','/assets/img/ocupado.png');
                      $(obj).data('status','ocupado');
                  }
                } 
              }
          });
        });
        $("#s_total").text(0);
        $("#s_as").text(0);
      });

      $("#selected_as").click(function(){
          boot.asientos.selected = [];
          boot.asientos.sarch(function(f,obj){
              if($(obj).data('status') == 'en proceso'){
                  boot.asientos.selected.push(f+$(obj).data('num'));
              }
          });
          if(boot.asientos.selected.length == 0){
              toastr.warning("Primero seleccione un asiento")
          }else{
            boot.asientos.save();
          }
      });
  },
  estados:{
      input:'#select_estado',
      key:'_estados',
      estados:localStorage.getItem(this.key),
      fill:function(){
          let select = this.input;
          $(select).empty();
          $.each(this.estados,function(i,estado){
             var $item = $('<option/>');
             $(select).append($item.val(estado.id).text(estado.estado));
          });
          __init.ciudades.get();
      },
      get:function(){
          let object = this;
          $.ajax({
              url:'/estados',
              method:'POST',
              dataType:'JSON'
          }).done(function(response){
              object.estados = response;
              localStorage.setItem(this.key,JSON.stringify(this.estados));
              object.fill();
          }).fail(function(error){
              console.error(error);
          });
      },
      isset:function(){
          if(this.estados === 'undefined' || this.estados === null || this.estados === '' || this.estados === 'null'){
              this.get();
          }else{
              this.estados = JSON.parse(this.estados);
              this.fill();
          }
      }
  },
  ciudades:{
      input:'#select_ciudades',
      fill:function(){
          let select = this.input;
          $(select).empty();
          $.each(this.ciudades,function(i,municipio){
             var $item = $('<option/>');
             $(select).append($item.val(municipio.id).text(municipio.municipio));
          });
          __init.sucursales.get();
      },
      get:function(){
          let object = this;
          $.ajax({
              url:'/ciudades',
              method:'POST',
              dataType:'JSON',
              beforeSend:function(xhr){
                  $(object.input).prop('disabled',true);
              },
              data:{id:$(__init.estados.input).val()}
          }).done(function(response){
              $(object.input).prop('disabled',false);
              object.ciudades = response;
              object.fill();
          }).fail(function(error){
              console.error(error);
          });
      }
  },
  sucursales:{
      input:'#select_sucursales',
      fill:function(){
          let select = this.input;
          $(select).empty();
          $.each(this.sucursales,function(i,sucursal){
             var $item = $('<option/>');
             $(select).append($item.val(sucursal.id).text(sucursal.nombre));
          });
          __init.cartelera.get();
      },
      get:function(){
          let object = this;
          $.ajax({
              url:'/sucursales',
              method:'POST',
              dataType:'JSON',
              beforeSend:function(xhr){
                  $(object.input).prop('disabled',true);
              },
              data:{id:$(__init.ciudades.input).val()}
          }).done(function(response){
              $(object.input).prop('disabled',false);
              object.sucursales = response;
              object.fill();
          }).fail(function(error){
              console.error(error);
          });
      }
  },
  cartelera:{
      div:"#cartelera",
      design:function(dts){
          return '<div class="row">'+
                          '<div class="col-lg-2 mb-r">'+
                          '</div>'+
                          '<div class="col-lg-2 mb-r">'+
                              '<div class="view overlay hm-white-slight">'+
                                  '<img src="assets/img/peliculas/'+dts.imagen+'" class="">'+
                                  '<a href="#" data-toggle="modal" data-target="#quick-look-ex">'+
                                      '<div class="mask waves-light waves-effect waves-light">'+
                                          '<div class="flex-center product-quick-look">'+
                                              '<i class="fa fa-eye fa-2x" aria-hidden="true"></i>'+
                                          '</div>'+
                                      '</div>'+
                                  '</a>'+
                              '</div>'+

                          '</div>'+
                          '<div class="col-lg-7 mb-r">'+

                              '<h4><a href="#">'+dts.nombre+'</a></h4>'+

                              '<p>'+dts.sinopsis+'</p>'+

                              '<div class="orange-text">'+
                                  '<i class="fa fa-star"> </i>'+
                                  '<i class="fa fa-star"> </i>'+
                                  '<i class="fa fa-star"> </i>'+
                                  '<i class="fa fa-star"> </i>'+
                                  '<i class="fa fa-star"> </i>'+
                              '</div>'+

                              '<p><span class="green-text price"><strong>dur: '+dts.duracion+'min. </strong></span></p>'+
                              '<div id="horarios_'+dts.id+'" class="list_horarios" data-info=\''+JSON.stringify(dts)+'\'></div><br><br>'+

                              '<a class="btn btn-outline-danger btn-rounded waves-effect" data-toggle="tooltip" data-placement="top" title="" href="'+dts.trailer+'" data-original-title="Add to Wishlist"><i class="fa fa-play" aria-hidden="true"></i> ver triller</a>'+

                          '</div>'+
                      '<hr>'
                      '</div>';
      },
      fill:function(){
          let object = this;
          let element = object.div;
          $(element).empty();
          $.each(this.cartelera,function(i,cartelera){
             let context = object.filterContext(cartelera.contextos.split(','));
             let horarios = object.filterHorarios(cartelera.contextos.split(','),cartelera.horas.split(','),context,cartelera.costos.split(','),cartelera.horarios_id.split(','));
             $(element).append(object.design(cartelera));
             $(element).find('#horarios_'+cartelera.id).append(object.createH(horarios));
          });
      },
      get:function(){
          let object = this;
          $.ajax({
              url:'/peliculas',
              method:'POST',
              dataType:'JSON',
              beforeSend:function(xhr){

              },
              data:{id:$(__init.sucursales.input).val()}
          }).done(function(response){
              object.cartelera = response;
              object.fill();
          }).fail(function(error){
              console.error(error);
          });
      },
      filterContext:function(context){
          var newContexts=[];
          $.each(context,function(i,c){
              var exist = 0;
              $.each(newContexts,function(i,nc){
                  if(c === nc)
                      exist = 1;
              });
              if(exist === 0){
                  newContexts.push(c);
              }
          });
          return newContexts;
      },
      filterHorarios:function(hisCntx,horarios,context,costos,horario_id){
          var filtered =[];
          $.each(context,function(i,c){
              var exist = 0;
              var object = {
                key:c,
                data:[]
              };
              $.each(filtered,function(i,f){
                  if(object.key === f.key)
                      exist = 1;
              });
              if(exist === 0){
                  $.each(horarios,function(i,h){
                      if(object.key === hisCntx[i]){
                          object.data.push({hora:h,precio:costos[i],horario_id:horario_id[i]});
                      }
                  });
              }
              filtered.push(object);
          });
          return filtered;
      },
      createH:function(horarios){
          var html = $('<div/>').addClass('horario');
          $.each(horarios,function(i,h){
              var label = $('<h5/>');
              var row = $('<div/>').addClass('row');
              row.append(label.text(h.key));
              $.each(h.data,function(i,hr){
                  var btnH = $('<a/>').addClass("btn btn-outline-info btn-rounded waves-effect btn_horario");
                  btnH.text(hr.hora);
                  btnH.data('precio',hr.precio);
                  btnH.data('id',hr.horario_id);
                  row.append(btnH);
              });
              html.append(row);
          });
          return html;

      }
  },
  asientos:{
      sarch:function(callback){
          $('.scenario').children('div').each(function(){ 
              $(this).children('div').eq(1).each(function(){
                  var f = $(this).data('fila');
                  $(this).children('img').each(function(i,a){
                      callback(f,a);
                  });
              }); 
          });
      },
      selected:[],
      data:function(){
          return {
              horario_id:$("#s_func").data('horario_id'),
              sucursal_id:$(__init.sucursales.input).val(),
              no_ass:__init.asientos.selected,
              total:$("#s_total").text()
          }
      },
      save:function(){
        return client.emit('buy',__init.asientos.data());
      },
      verifyBusy:function(){
        var hor_id = $("#s_func").data('horario_id');
        $.ajax({
          url:'/get-asientos',
          method:'GET',
          dataType:'JSON',
          data:{horario_id:hor_id}
        }).done(function(response){
          __init.asientos.sarch(function(f,obj){
            var num = f+$(obj).data('num');
            $.each(response,function(i,r){
                if($(__init.sucursales.input).val() == r.sucursal_id && r.hora == $("#s_func").text()){
                  if(r.no_as === num){
                    if(r.reserved){
                        $(obj).attr('src','/assets/img/ocupado.png');
                        $(obj).data('status','ocupado');
                    }
                  } 
                }
            });
          });
        }).fail(function(error){
          console.log(error);
        });
      },
      clear:function(){
        __init.asientos.sarch(function(f,obj){
              $(obj).attr('src','/assets/img/desocupado.png');
              $(obj).data('status','desocupado');
          });
        $("#s_total").text(0);
        $("#s_as").text(0);
      }
  }

};

$(__init.boot());
