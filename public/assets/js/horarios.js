$(function(){
	var object = {
		getPelicuas : function(){
			$.ajax({
				url:'/get-pelicuas',
				type:"post"
			}).done(function(response){
				console.log(response);
            	$.each(response,function(i,pelicula){
               		var $item = $('<option/>');
               		$("#peliculas").append($item.val(pelicula.id).text(pelicula.nombre));
            	});
			}).fail(function(error){
				console.error(error);
			})
		},
		getSalas : function(){
			$.ajax({
				url:'/get-salas',
				type:"post"
			}).done(function(response){
				console.log(response);
            	$.each(response,function(i,sala){
               		var $item = $('<option/>');
               		$("#salas").append($item.val(sala.id).text(sala.nombre));
            	});
			}).fail(function(error){
				console.error(error);
			})
		},
		getContextos : function(){
			$.ajax({
				url:'/get-contextos',
				type:"post"
			}).done(function(response){
				console.log(response);
            	$.each(response,function(i,contexto){
               		var $item = $('<option/>');
               		$("#contextos").append($item.val(contexto.id).text(contexto.nombre));
            	});
			}).fail(function(error){
				console.error(error);
			})
		}
	}
	$('.datepicker').pickadate({
	   // Escape any “rule” characters with an exclamation mark (!).
  format: 'yyyy-mm-dd',
  formatSubmit: 'yyyy/mm/dd',
  hiddenPrefix: 'prefix__',
  hiddenSuffix: '__suffix'
	}); // init data picker mdb
	$('#hora').pickatime({ //init time picjer mdb
    	// 12 or 24 hour 
	    twelvehour: true,
	    // Light or Dark theme
	    darktheme: true
	});
	object.getPelicuas();
	object.getSalas();
	object.getContextos();
	$('.mdb-select').material_select(); // init material select
	$("#btn-upload-hora").click(function(event){
		var horaFormat = $("#hora").val();
		var hora   = horaFormat.substring(0,5);
		var format = horaFormat.substring(5,8) 
		if(format=="PM"){//aquí ya valio...
			hor2 = hora.substring(0,2);
			hor2 = parseInt(hor2); 
			if(hor2<8){
				hor2 = hor2+2;
				hor2 = "1"+hor2;
			}else{
				hor2 = hor2+2;
				hor2 = hor2-10;
				hor2 = "2"+hor2;
			}
			hora = hor2+hora.substring(2,5);
			console.log(hora);

		}
		data = {
			"pelicula" : $("#peliculas").val(),
			"sucursal" : $("#select_sucursales").val(),
			"sala"	   : $("#salas").val(),
			"fecha"	   : $("#fecha").val(),
			"hora"	   : hora,
			"contexto" : $("#contextos").val()
		};
		console.log(data)
		$.ajax({
			url:"/horarios-reg",
			type:"POST",
			data:data
		}).done(function(response){
			console.log(response);
			if(response.status ==401){
				$.each(response.data,function(i,o){
					toastr.warning(o.message);
				});
			}else if(response.status==200){
				toastr.success("Registro realizado exitosamente");
			}else if(response.status==402){
				toastr.warning(response.message);
			}else{
				toastr.error("Algo salio mal consulte con el administrador del portal");
			}
		}).fail(function(error){
			console.error(error);
		});
	});
});
