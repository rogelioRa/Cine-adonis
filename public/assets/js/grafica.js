$(function () { 
	var series = {
		values : [],
		keys: []
	}
	function updateGrafica(series,type){
		var myChart = Highcharts.chart('grafica', {
		    chart: {
		        type: type
		    },
		    title: {
		        text: 'Ventas de la semana'
		    },
		    xAxis: {
		        categories: series.keys
		    },
		    yAxis: {
		        title: {
		            text: 'Ventas'
		        }
		    },
		    series: [{
		        name: 'Ventas',
		        data: series.values
		    }]
		});
	}
	function getData(){
		$.ajax({
			"url":"getEstadisticas",
			"type":"get",
		}).done(function(response){
			console.log(response);
			formatData(response);
			console.log(series);
			updateGrafica(series);
		}).fail(function(error){
			console.error(error);
		});
	}
	function formatData(data){
		series.values = [];
		series.keys = [];
		$.each(data[0],function(clave,valor){
			series.values.push(parseInt(valor));
			series.keys.push(clave);
		});
	}
	getData();
	client.on('buy',function(buyed){ // cuando se realice una compra actualizamos las estadíasticas
    	getData();     
  	});
});