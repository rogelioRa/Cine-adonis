$(function(){
	function register(data){
		console.log(data)
		$.ajax({
			'url':"/register",
			"type":"POST",
			"data":data
		}).done(function(response){
			console.log(response);
			if(response.status==401){
				$.each(response.data,function(i,o){
					toastr.warning(o.message);
				});
			}else if(response.status==200){
				toastr.success("Registro realizado exitosamente");
			}else{
				toastr.error("Registro realizado exitosamente");
			}
		}).fail(function(error){
			toastr.error("Algo salio mal consulta con el administrador");
			console.log(error);
		});
	};
	function login(data){
		$.ajax({
			"url":"/login",
			"type":"post",
			"data":data
		}).done(function(response){
			console.log(response);
			toastr.success("Bienvenido "+data.email);
			$('#modalRegister').modal('hide');
            document.location.reload();
		}).fail(function(error){
			console.error(error);
			toastr.warning("Credenciales incorrectas");
		});
	}
	$("#btn-login").click(function(){
		data = {
			'email':$("#email").val(),
			'password':$("#password").val()
		};
		login(data);
	});
	$("#btn-register").click(function(){
		data = {
			'username':$("#username-r").val(),
			'email':$("#email-r").val(),
			'password':$("#password-r").val(),
			"cpassword":$("#password-rc").val()
		};
		register(data)
	});
});
