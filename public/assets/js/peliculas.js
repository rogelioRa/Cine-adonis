let __init_p = {
    boot:function(){
        let boot=this;
        boot.clasificaciones.get();
        boot.generos.get();
        $("#btn-save").on({
            click:function(){
                boot.peliculas.save();
            }
        });

    },
    peliculas:{
        data:function(){
            var frmDta = new FormData($('#frm_imagen')[0]);
            frmDta.append('nombre',$("#nombre").val());
            frmDta.append('duracion',$("#duracion").val());
            frmDta.append('sinopsis',$("#sinopsis").val());
            frmDta.append('trailer',$("#trailer").val());
            frmDta.append('clasificacion_id',$("#select_clasificaciones").val());
            frmDta.append('genero_id',$("#select_generos").val());
            return frmDta;
        },
        save:function(){
            console.log(__init_p.peliculas.data());
            $.ajax({
                url:'pelicula/save',
                method:'POST',
                data:__init_p.peliculas.data(),
                processData:false,
                contentType:false
            }).done(function(response){
                console.log(response);
                if(response.status == 200){
                    __init_p.frm.clear();
                    toastr.success("Pelicula registrada");
                }
                else
                    toastr.success("Ups! Algo salio mal.");
            }).fail(function(error){
                console.log(error);
            });
        }
    },
    clasificaciones:{
        input:'#select_clasificaciones',
        key:'_clasificaciones',
        clasificaciones:localStorage.getItem(this.key),
        fill:function(){
            let select = this.input;
            $(select).empty();
            console.log(this.clasificaciones);
            $.each(this.clasificaciones,function(i,clasificacion){
               var $item = $('<option/>');
               $(select).append($item.val(clasificacion.id).text(clasificacion.nombre));
            });
        },
        get:function(){
            let object = this;
            $.ajax({
                url:'/clasificaciones',
                method:'POST',
                dataType:'JSON'
            }).done(function(response){
                object.clasificaciones = response;
                localStorage.setItem(this.key,JSON.stringify(this.clasificaciones));
                object.fill();
            }).fail(function(error){
                console.error(error);
            });
        }
    },
    generos:{
        input:'#select_generos',
        key:'_generos',
        generos:localStorage.getItem(this.key),
        fill:function(){
            let select = this.input;
            $(select).empty();
            $.each(this.generos,function(i,genero){
               var $item = $('<option/>');
               $(select).append($item.val(genero.id).text(genero.nombre));
            });
        },
        get:function(){
            let object = this;
            $.ajax({
                url:'/generos',
                method:'POST',
                dataType:'JSON'
            }).done(function(response){
                object.generos = response;
                localStorage.setItem(this.key,JSON.stringify(this.generos));
                object.fill();
            }).fail(function(error){
                console.error(error);
            });
        }
    },
    frm:{
      clear:function(){
        $("#frm_peli")[0].reset();
      }
    }

};

$(__init_p.boot());
